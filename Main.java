// По оформлению кода прошу сильно не занижать оценку. Требований к оформлению кода не было, поэтому написал как было удобнее мне 
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
//        p1(); - Done
//        p2(); - Done
//        p3(); - Done
//        p4(); - Done
//        p5(); - Done
//        p6(); - Done
//        p7(); - Done
//        p8(); - Done
    }


    private static void p1() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите число x: ");
        Double x = scanner.nextDouble();
        System.out.print("Введите число y: ");
        Double y = scanner.nextDouble();

        System.out.println("Сумма:" + (x + y));
        System.out.println("Разница x и y:" + (x - y));
        System.out.println("Деление x на y:" + (x / y));
        System.out.println("Умножение:" + (x * y));

    }

    private static void p2() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите число a: ");
        Double a = scanner.nextDouble();
        System.out.print("Введите число b: ");
        Double b = scanner.nextDouble();
        System.out.print("Введите число c: ");
        Double c = scanner.nextDouble();

        Double z = ((a-3)*(b/2)+c);
        System.out.println(z);
    }

    private static void p3() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите число a: ");
        Double a = scanner.nextDouble();
        System.out.print("Введите число b: ");
        Double b = scanner.nextDouble();
        System.out.print("Введите число c: ");
        Double c = scanner.nextDouble();

        Double output = ((b+Math.sqrt(Math.pow(b,2)+4*a*c))/2*a)- Math.pow(a,3)+Math.pow(b,-2);
        System.out.println(output);
    }

    private static void p4() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите сторону 1: ");
        Double a = scanner.nextDouble();
        System.out.print("Введите сторону 2: ");
        Double b = scanner.nextDouble();

        Double c =  Math.sqrt((Math.pow(a,2))+Math.pow(b,2));
        Double perimtr = a + b + c;
        Double area = a * b * c;

        System.out.println("Периметр: " + perimtr);
        System.out.println("Площадь: " + area);

    }

    private static void p5() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите x1: ");
        Double x1 = scanner.nextDouble();
        System.out.print("Введите y1: ");
        Double y1 = scanner.nextDouble();

        System.out.print("Введите x2: ");
        Double x2 = scanner.nextDouble();
        System.out.print("Введите y2: ");
        Double y2 = scanner.nextDouble();

        Double lenght = Math.sqrt(Math.pow((x2-x1),2) + Math.pow((y2-y1),2));
        System.out.println("Растояние между двумя точками : " + lenght);
    }
    private static void p6() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите радиус круга: ");
        Double radius=scanner.nextDouble();
        Double circumference =(Math.PI*(radius*radius));
        System.out.print("Площадь круга: "+circumference);

        Double area =(Math.PI*2*radius);
    }

    private  static void p7(){
        System.out.print("Четвертая степень числа PI: "+ Math.pow(Math.PI,4));

    }
    private  static void p8(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int n=scanner.nextInt();
        int product = 1;

        while (n != 0) {
            product = product * (n % 10);
            n = n / 10;
        }
        System.out.print(product);

    }


}

